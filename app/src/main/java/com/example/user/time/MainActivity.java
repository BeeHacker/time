package com.example.user.time;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

public class MainActivity extends AppCompatActivity {
 private TimePicker tp;
    private Button btnGetRes;
    private TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tp=(TimePicker) findViewById(R.id.timePicker);
        btnGetRes=(Button)findViewById(R.id.btnFindResult);
        tvResult=(TextView)findViewById(R.id.txtResult);
        btnGetRes.setOnClickListener(new View.OnClickListener(){
            @Overridepublic void onClick(View view){
                int h, m;
                if(android.os.Build.VERSION.Sdk_int>=android.os.Build.VERSION_CODES.M) {
                    h = tp.getHour();
                    m = tp.getMinute();
                }else{
                    h=tp.getCurrentHour();
                    m=tp.getCurrentMinute();
                }
                double a=AngleEvaluator.getAngle(h,m);
                tvResult.setText(getString(R.string.result, a));
            }
        })
        /*setContentView(R.layout.activity_main);
        tp=(TimePicker)findViewById(R.id.btn)*/
    }
}
