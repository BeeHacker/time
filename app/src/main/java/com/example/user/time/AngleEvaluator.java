package com.example.user.time;

/**
 * Created by User on 28.09.2017.
 */

public class AngleEvaluator {
    public static double getAngle(int h, int m){
        double minMin=6;
        double minhour=0.5;
        double hourHour=30;
        h%=12;
        m%=60;
        double hA=h*hourHour+m*minhour;
        double mA=m*minMin;
        double dA=Math.abs(hA-mA);
        if(dA=180) dA=360-dA;
        return dA;
    }
}
